import pygame

pygame.init()
display_width = 920
display_height = 600

white = (255, 255, 255)
black = (0, 0, 0)
dark_blue = (0, 50, 255)
light_blue = (0, 50, 230)
def text_objects(text, font):
    text_surface = font.render(text, True, black)
    return text_surface, text_surface.get_rect()

# need to create the window
game_display = pygame.display.set_mode([display_width, display_height])

def button(message, x_value, y_value, width, height, active_color, inactive_color, action=None):
    mouse_posistion = pygame.mouse.get_pos()
    mouse_click = pygame.mouse.get_pressed()

    if x_value + width > mouse_posistion[0] > x_value and y_value + height > mouse_posistion[1] > y_value:
        pygame.draw.rect(game_display, active_color, (x_value, y_value, width, height))
        if mouse_click[0] == 1 and action != None:
            action()
    else:
        pygame.draw.rect(game_display, inactive_color, (x_value, y_value, width, height))
    button_font = pygame.font.SysFont(None, 25)
    button_text, button_area = text_objects(message, button_font)
    button_area.center = ((x_value + (width/2)), (y_value + (height/2)))
    game_display.blit(button_text, (button_area))


def done_or_not(board):
    # each row needs to have 9 unique numbers with the largest being 9 and the smallest 1
    for row in board:
        if len(set(row)) == 9 and min(row) == 1 and max(row) == 9:
            pass
        else:
            return False
    # each column needs to have 10 unique numbers with the largest being 9 and smallest 0
    # loop through each column
    for x in range(len(board[0])):
        # create a variable to hold the characters for that column
        unique_chars = []
        # loop through each row to get the value for the current column on each row
        for row in board:
            # add the value of the rows x rolumn
            unique_chars.append(row[x])
            # check if have looped through the whole row
            if len(unique_chars) == 9:
                # check if the length of a set (returns unique) still equals 10 and the min and max
                if len(set(unique_chars)) == 9 and min(unique_chars) == 1 and max(unique_chars) == 9:
                    pass
                else:
                    return False
    # each region needs to have 10 unique numbers with the largest being 9 and smallest 0
    # manually do each region? I can't think of another way
    # create 9 variables, each for a specific region
    region1 = []
    region2 = []
    region3 = []
    region4 = []
    region5 = []
    region6 = []
    region7 = []
    region8 = []
    region9 = []
    # loop through each row but break them down into slice for each set of regions (1-3) (4-6) (7-9)
    for row in board[0:3]:
        # isolated the rows for region 1, 2 and 3
        # need to take into account duplicate rows so cannot use row == board[x]
        # append each indivudal item associated with the specific region
        # need to get the slice of columns for each region
        # use extend method to combine the return for the slice with the associated region
        region1.extend(row[0:3])
        region2.extend(row[3:6])
        region3.extend(row[6:9])
    for row in board[3:6]:
        region4.extend(row[0:3])
        region5.extend(row[3:6])
        region6.extend(row[6:9])
    for row in board[6:9]:
        region7.extend(row[0:3])
        region8.extend(row[3:6])
        region9.extend(row[6:9])
    # create and fill a region list with each region 
    region_list =[]
    region_list.append(region1)
    region_list.append(region2)
    region_list.append(region3)
    region_list.append(region4)
    region_list.append(region5)
    region_list.append(region6)
    region_list.append(region7)
    region_list.append(region8)
    region_list.append(region9)
    # loop through each region to make sure that it fulfills the requirements
    for region in region_list:
        if len(set(region)) == 9 and min(region) == 1 and max(region) == 9:
            pass
        else:
            return False
    # if the code makes it to this point it has passed all of the tests
    return True


   

def game_intro():
    intro = True
    while intro == True:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    quit()
        game_display.fill(white)
        main_screen_font = pygame.font.SysFont(None, 115)
        game_name, game_title_box = text_objects("Sudoku", main_screen_font)
        game_title_box.center = (display_width/2, 40)
        escape_font = pygame.font.SysFont(None, 30)
        escape_notice, useless = text_objects("Press escape to exit game", escape_font)
        game_display.blit(escape_notice, (5, display_height-25))
        game_display.blit(game_name, game_title_box)
        button("Board 1", 0, 60, 100, 50, dark_blue, light_blue, board_1)
        pygame.display.update()             


# def create_start_grid(board):




def game_loop(board, num):
    # before while loop need to get the index of index where values are not None, use these to make sure player is not trying to overwrite the built in values
    # create a variable to hold indexs that cannot be changed
    unchangeables = []
    for row in board:
        for item in row:
            if item != None:
                row_index = board.index(row) 
                item_index = row.index(item)
                unchangeables.append((row_index, item_index))
    # as long as done_or_not returns false we continue, need to create the board before this though
    print(unchangeables)
    while done_or_not(board) == False:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    quit()
        game_display.fill(white)
        board_name_font = pygame.font.SysFont(None, 50)
        board_name_text, board_name_rect = text_objects("Board " + str(num), board_name_font)
        # .center means the center of the rectangle is at this point the, usually the top left is what determines where an object is placed
        board_name_rect.center = (display_width / 2, 15)
        game_display.blit(board_name_text, board_name_rect)
        escape_font = pygame.font.SysFont(None, 30)
        escape_notice, useless = text_objects("Press escape to exit game", escape_font)
        game_display.blit(escape_notice, (5, display_height-25))
        pygame.draw.line(game_display, black, (8, 40), (display_width - 8, 40), 2)
        first_line = 20
        diff = ((display_width-40)/ 9)
        print(diff)
        height_diff = ((display_height - 85)/9)
        print(height_diff)
        for number in range(10):
            pygame.draw.line(game_display, black, ((diff * number) + first_line, 60), ((diff * number) + first_line, display_height -26), 2)
            pygame.draw.line(game_display, black, (20, (height_diff * number) + 60), (display_width - 20, (height_diff * number) + 60), 2)
            if number < 10:
                button
        pygame.display.update()
    

def board_1():
    # need to create list of lists, each list represents a row of the board, if I want more boards I could change this to a general function
    board_1 = [
        [None, None, 4, None, 5, None, None, None, None],
        [9, None, None, 7, 3, 4, 6, None, None],
        [None, None, 3, None, 2, 1, None, 4, 9],
        [None, 3, 5, None, 9, None, 4, 8, None],
        [None, 9, None, None, None, None, None, 3, None],
        [None, 7, 6, None, 1, None, 9, 2, None],
        [3, 1, None, 9, 7, None, 2, None, None],
        [None, None, 9, 1, 8, 2, None, None, 3],
        [None, None, None, None, 6, None, 1, None, None]
    ]
    game_loop(board_1, 1)


game_intro()